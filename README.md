# Magisk Module: Xperiance SmartCharger

This module brings back Battery Care to Xperia Devices with LineageOS by adding a vendor platform signature using a Runtime Resource Overlay (RRO) so the app can run with `android:sharedUserId="android.uid.system"`.

For further information please check https://review.lineageos.org/q/Ida9bb25a32234af9d9507a214eae6a4672320d2b

## Build

You can build the module on your own by running `./gradlew packageMagiskModule`. The module zip file can be found in `build/outputs/magisk`.
